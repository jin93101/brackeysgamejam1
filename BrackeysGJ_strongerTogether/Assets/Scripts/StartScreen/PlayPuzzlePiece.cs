﻿using UnityEngine;
using UnityEngine.UI;

public class PlayPuzzlePiece : MonoBehaviour
{
    private Vector3 mousePosition;
    private float _moveSpeed = 1f;

    public string pieceStatus = "idle";

    public Text instructionForLeftPiece;
    public Text putMeHere;

    private bool _instructionON = false;
    public GameObject rightMouseInstruction;
    public GameObject arrow;
    public GameObject longArrow;

    // Update is called once per frame
    void Update()
    {
        //move the puzzle piece with mouse cursor
        if (pieceStatus == "pickedUp")
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, _moveSpeed);
            putMeHere.text = "Put me Here";
        }

        if(pieceStatus == "placed")
        {
            instructionForLeftPiece.text = "";
            putMeHere.text = "";
        }

        //right mouse button to place the piece 
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            pieceStatus = "placed";
        }
    }

    private void OnMouseDown()
    {
        pieceStatus = "pickedUp";
        if(_instructionON == false)
        {
            Instantiate(rightMouseInstruction, new Vector2(-3.45f, 1.88f), Quaternion.identity);
            Instantiate(arrow, new Vector2(-2.45f, 1.89f), Quaternion.identity);
            Instantiate(longArrow, new Vector3(3.77f, 2.16f, -3f), Quaternion.identity);
            _instructionON = true;
        }
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if ((collision.gameObject.name == gameObject.name) && (pieceStatus == "pickedUp"))
        {
            //Debug.Log("Do we collide?");
            instructionForLeftPiece.text = "Use right mouse button to place the puzzle piece";
        }
    }
}
