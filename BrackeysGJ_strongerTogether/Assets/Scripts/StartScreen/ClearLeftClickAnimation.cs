﻿using UnityEngine;

public class ClearLeftClickAnimation : MonoBehaviour
{
  
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Destroy(gameObject);
        }
    }
}
