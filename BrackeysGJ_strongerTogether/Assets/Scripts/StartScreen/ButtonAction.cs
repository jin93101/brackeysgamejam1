﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonAction : MonoBehaviour
{
    public void LoadNextScene()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene + 1);
    }

    public void LoadPreviousScene()
    {
        PuzzlePiece.score = 0;
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene - 1);
    }

    public void LoadRandomLevel()
    {
        int rand = Random.Range(0, 2);
        switch (rand)
        {
            case 1:
                PuzzlePiece.score = 0;
                SceneManager.LoadScene("Level1");
                break;
            case 2:
                PuzzlePiece.score = 0;
                SceneManager.LoadScene("Level3");
                break;
            case 0:
                PuzzlePiece.score = 0;
                SceneManager.LoadScene("Level4");
                break;
        }
    }

    public void LoadLevel1()
    {
        PuzzlePiece.score = 0;
        SceneManager.LoadScene("Level1");
    }

    public void LoadLevel3()
    {
        PuzzlePiece.score = 0;
        SceneManager.LoadScene("Level3");
    }

    public void LoadLevel4()
    {
        PuzzlePiece.score = 0;
        SceneManager.LoadScene("Level4");
    }
}
