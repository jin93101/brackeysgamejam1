﻿using UnityEngine;

public class UpControlArrow : MonoBehaviour
{
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            anim.SetBool("pressed", true);
        }
        else
        {
            anim.SetBool("pressed", false);
        }
    }
}
