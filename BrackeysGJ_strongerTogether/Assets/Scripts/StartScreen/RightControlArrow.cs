﻿using UnityEngine;

public class RightControlArrow : MonoBehaviour
{
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetBool("pressed", true);
        }
        else
        {
            anim.SetBool("pressed", false);
        }
    }
}
