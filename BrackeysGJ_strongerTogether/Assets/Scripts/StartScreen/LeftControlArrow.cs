﻿using UnityEngine;

public class LeftControlArrow : MonoBehaviour
{
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            anim.SetBool("pressed", true);
        }
        else
        {
            anim.SetBool("pressed", false);
        }
    }
}
