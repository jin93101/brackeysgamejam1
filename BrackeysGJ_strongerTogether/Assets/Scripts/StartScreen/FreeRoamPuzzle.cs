﻿using UnityEngine;

public class FreeRoamPuzzle : MonoBehaviour
{
    private Vector3 mousePosition;
    private float _moveSpeed = 1f;
    public string pieceStatus = "idle";

    // Update is called once per frame
    void Update()
    {
        if (pieceStatus == "pickedUp")
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, _moveSpeed);
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            pieceStatus = "placed";
        }
    }

    private void OnMouseDown()
    {
        pieceStatus = "pickedUp";
    }
}
