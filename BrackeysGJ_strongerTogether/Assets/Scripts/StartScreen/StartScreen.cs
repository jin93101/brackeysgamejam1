﻿using UnityEngine;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    public Text welcomeMessage;
    public Text playButtonText;
    public Text instructionsText01;
    public Text instructionsText03;
    public Text instructionsText04;

    private void Start()
    {
        welcomeMessage.text = "! Welcome !";
        playButtonText.text = "PLAY";
        instructionsText01.text = "Pick me!! -->" +
            " Use Left Mouse ";
        instructionsText03.text = "Use your arrows to move the character";
        instructionsText04.text = "Kind advice. Be fast.";
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            instructionsText01.text = "";
        }
    }


}
