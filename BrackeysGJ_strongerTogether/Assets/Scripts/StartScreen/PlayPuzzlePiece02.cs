﻿using UnityEngine;
using UnityEngine.UI;

public class PlayPuzzlePiece02 : MonoBehaviour
{
    private Vector3 mousePosition;
    private float _moveSpeed = 1f;

    public int mouseCounter = 0;

    public string pieceStatus = "idle";

    public Text instructionsForRightPiece;
    public GameObject playButton;
    public GameObject tutorialSection;

    // Update is called once per frame
    void Update()
    {
        //move the puzzle piece with mouse cursor
        if (pieceStatus == "pickedUp")
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, _moveSpeed);
        }

        if ((mouseCounter == 2))
        {
            playButton.gameObject.SetActive(true);
            tutorialSection.gameObject.SetActive(true);
            pieceStatus = "2ndIsPlaced";
            mouseCounter = 3;
            instructionsForRightPiece.text = "<-- Now you can press this";
        } else
        {
            instructionsForRightPiece.text = "";
        }

        //right mouse button to place the piece 
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            mouseCounter++;
            pieceStatus = "2ndIsPlaced";
        }
    }

    private void OnMouseDown()
    {
        pieceStatus = "pickedUp";
    }
}
