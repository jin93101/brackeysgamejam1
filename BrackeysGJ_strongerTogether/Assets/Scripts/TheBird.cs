﻿using UnityEngine;

public class TheBird : MonoBehaviour
{
    public float speed;
    public float endXpos;

    public GameObject hitWithBird;
    public GameObject destroySound;

    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        KillMe();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(destroySound, transform.position, Quaternion.identity);
            Instantiate(hitWithBird, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }

    private void KillMe()
    {
        if (transform.position.x <= endXpos)
        {
            PuzzlePiece.score += 3;
            Destroy(gameObject);
        }
    }
}
