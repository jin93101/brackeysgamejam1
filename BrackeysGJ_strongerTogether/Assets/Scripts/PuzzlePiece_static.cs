﻿using UnityEngine;

public class PuzzlePiece_static : MonoBehaviour
{
    /*
     *
     *This script is for the pieces that will remain stationary
     * 
     */
    private Vector3 mousePosition;
    private float _moveSpeed = 1f;

    public static bool pieceInHand = false;
    public float angle = 90f;

    private string checkPlacement = "no";
    public string pieceStatus = "idle";
    /* available piece Statuses :
     * 
     * idle 
     * getBack
     * pickedUp
     * placed
     * 
    */

    //public float _timer;
    //public float _startTimer = 3f;

    public float timer;
    public float startTimer = 15f;

    //positions where the pieces return if droped
    private Vector2 target = new Vector2(-6f, -3f);
    private float speed = 100;


    private void Update()
    {
        //move the piece back to the pack of pieces positioned in the left bottom corner of the screen
        if (pieceStatus == "getBack")
        {
            transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * speed);
        }
        //move the puzzle piece with mouse cursor
        if(pieceStatus == "pickedUp")
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, _moveSpeed);
        }

        //right mouse button to place the piece 
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            checkPlacement = "yes";

            if(pieceStatus == "pickedUp")
            {
                pieceStatus = "getBack";
            }
        }
    }

    private void OnMouseDown()
    {
        pieceStatus = "pickedUp";
        checkPlacement = "no";
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if ((collision.gameObject.name == gameObject.name) && (checkPlacement == "yes"))
        {
            transform.position = collision.gameObject.transform.position;
            pieceStatus = "placed";
            checkPlacement = "no";
        }
     }
    /*
    private void RotateThePiece()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            transform.eulerAngles = new Vector3(90, 0, 0);
            Debug.Log("Rotate");
        }
    }*/
}
