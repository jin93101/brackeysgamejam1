﻿using UnityEngine;

public class DeadlySpikes : MonoBehaviour
{
    private Animator anim;
    public GameObject col;
    private float _timer = 2f;
    private float _destroyTimer = 2f;
    private bool _readyToBeDestroyed = false;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if (_timer > 0)
        { _timer -= Time.deltaTime; }
        else
        {
            anim.SetBool("attack", true);
            col.SetActive(true);
            _readyToBeDestroyed = true;
        }
        if(_readyToBeDestroyed == true)
        {
            if(_destroyTimer > 0)
            {
                _destroyTimer -= Time.deltaTime;
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
