﻿using UnityEngine;

public class HeartPickUp : MonoBehaviour
{
    private float _timer = 5f;
    public GameObject heartIsPickedUp;
    public GameObject hitTheHeartSound;

    private void Update()
    {
        if(_timer > 0)
        {
            _timer -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(hitTheHeartSound, transform.position, Quaternion.identity);
            Instantiate(heartIsPickedUp, new Vector2(transform.position.x, transform.position.y + 1f), Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
