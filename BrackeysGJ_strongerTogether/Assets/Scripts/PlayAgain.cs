﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayAgain : MonoBehaviour
{
    public Text scoreDisplay;
    public Text gameOverText;

    private void Start()
    {
        gameOverText.text = "You ran out of lives. Tough luck!";
    }

    void Update()
    {
        scoreDisplay.text = "Your Score is: " + PuzzlePiece.score.ToString();

        if (Input.GetKeyDown(KeyCode.Return))
        {
            PuzzlePiece.score = 0;
            SceneManager.LoadScene("Level1");
        }    
    }
}
