﻿using UnityEngine;

public class SpikeCollider : MonoBehaviour
{
    public GameObject destroySound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(destroySound, transform.position, Quaternion.identity);
        }
    }
}
