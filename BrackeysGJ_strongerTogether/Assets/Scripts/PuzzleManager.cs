﻿using UnityEngine;
using UnityEngine.UI;

public class PuzzleManager : MonoBehaviour
{

    public Text displayScore;
    public Text droppedPieces;

    //info texts about changes in the game
    public Text infoTextBox;
    //private bool infoAboutChangeInTheGame = false;
    //TODO
    /*
     * Other Changes
     * - more lifes
     * - deadly spikes
     * - double jump
     * - inverted visibility + viísible timer
     * - invulnerability + visible timer
     * 
     * public Text visibleTimer;
     * 
     */

    void Update()
    {
        displayScore.text = "score: " + PuzzlePiece.score.ToString();
        droppedPieces.text = "Dropped Pieces Here";

        if((PuzzlePiece.score >= 100) && (PuzzlePiece.score < 120)) 
        {
            infoTextBox.text = "New enemy: Bird!";
        }
        else if ((PuzzlePiece.score >= 125) && (PuzzlePiece.score < 150))
        {
            infoTextBox.text = "New threat: Deadly Spikes!";
        }
        else
        {
            infoTextBox.text = "";
        }

    }

}
