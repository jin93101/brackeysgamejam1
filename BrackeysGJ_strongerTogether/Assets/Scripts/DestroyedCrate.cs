﻿using UnityEngine;

public class DestroyedCrate : MonoBehaviour
{
    public float speed;
    public float endXpos;

    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        KillMe();
    }

    private void KillMe()
    {
        if (transform.position.x <= endXpos)
        {
            Destroy(gameObject);
        }
    }
}
