﻿using UnityEngine;
using UnityEngine.UI;

public class PuzzlePiece : MonoBehaviour
{
    private Vector3 mousePosition;
    private float _moveSpeed = 1f;

    public static bool pieceInHand = false;
    public float angle = 90f;

    private string checkPlacement = "no";
    public string pieceStatus = "idle";
    /* available piece Statuses :
     * 
     * idle 
     * getBack
     * pickedUp
     * placed
     * 
    */

    public float timer;
    public float startTimer = 15f;

    //positions where the pieces return if droped
    private Vector2 target = new Vector2(-6f, -3f);
    private float speed = 100;

    public static int score;

    private void Update()
    {
        if(pieceStatus == "placed")
        {
            TimerIsSet();
        } 

        //move the piece back to the pack of pieces positioned in the left bottom corner of the screen
        if (pieceStatus == "getBack")
        {
            transform.position = Vector2.MoveTowards(transform.position, target, Time.deltaTime * speed);
        }
        //move the puzzle piece with mouse cursor
        if(pieceStatus == "pickedUp")
        {
            mousePosition = Input.mousePosition;
            mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
            transform.position = Vector2.Lerp(transform.position, mousePosition, _moveSpeed);
        }

        //right mouse button to place the piece 
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            checkPlacement = "yes";

            if (pieceStatus == "pickedUp")
            {
                pieceStatus = "getBack";
            }
        }
    }

    private void OnMouseDown()
    {
        pieceStatus = "pickedUp";
        checkPlacement = "no";
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if ((collision.gameObject.name == gameObject.name) && (checkPlacement == "yes"))
        {
            transform.position = collision.gameObject.transform.position;
            pieceStatus = "placed";
            score += 1;
            checkPlacement = "no";
        }
     }

    private void TimerIsSet()
    {
        int numb = Random.Range(0, 3);
        switch(numb)
        {
            case 0:
                startTimer = 10;
                break;
            case 1:
                startTimer = 15;
                break;
            case 2:
                startTimer = 20;
                break;
            case 3:
                startTimer = 25;
                break;
        }

        if (timer > 0)
        { timer -= Time.deltaTime; }
        else
        {
            timer = startTimer;
            pieceStatus = "getBack";
        }
    }
}
