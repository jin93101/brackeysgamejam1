﻿using UnityEngine;

public class SoundController : MonoBehaviour
{
    private AudioSource audioSource;
    public AudioClip[] soundCollection;
    public int rand;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        
        if (!audioSource.isPlaying)
        {
            rand = Random.Range(0, soundCollection.Length);
            audioSource.clip = soundCollection[rand];
            audioSource.Play();
        }
    }
}
