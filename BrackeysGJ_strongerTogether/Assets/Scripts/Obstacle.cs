﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float speed;
    public float endXpos;

    public GameObject destroyedCrate;
    public GameObject destroySound;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.left * speed * Time.deltaTime);
        KillMe();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Instantiate(destroySound, transform.position, Quaternion.identity);
            Instantiate(destroyedCrate, new Vector2(transform.position.x, -0.07f), Quaternion.identity);
            Destroy(gameObject);
        }
    }

    private void KillMe()
    {
        if(transform.position.x <= endXpos)
        {
            PuzzlePiece.score += 2;
            Destroy(gameObject);
        }
    }
}
