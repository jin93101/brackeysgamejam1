﻿using UnityEngine;

public class runningBackground : MonoBehaviour
{

    public float speed;
    public float endX;
    public float startX;
    //z position is to set the layer order -18.09 18.03
    public float zPositon;

    void Update()
    {
        //move the background to the left at desired speed
        transform.Translate(Vector2.left * speed * Time.deltaTime);

        if(transform.position.x <= endX)
        {
            //having vector 3 here and I need to set the Z coord in order to keep the layers in order
            Vector3 pos = new Vector3(startX, transform.position.y, zPositon);
            transform.position = pos;
        }
    }
}
