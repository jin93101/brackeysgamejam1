﻿using UnityEngine;

public class CleaningScript : MonoBehaviour
{
    private float _killMe = 2f;

    private void Update()
    {
        _killMe -= Time.deltaTime;
        if(_killMe <= 0)
        {
            Destroy(gameObject);
        }
    }
}
