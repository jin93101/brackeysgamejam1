﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    //horizontal movement
    private float _frontSpeed = 2f;
    private float _backSpeed = 4f;

    //health section
    public int hearts;
    public Text numberOfHeats;

    //jump section
    private bool _isGrounded;
    public Transform groundCheck;
    public float checkRadius;
    public LayerMask thisIsGround;
    private float _jumpSpeed = 7f;
    //double jump prep
    private int _doubleJump;
    private int _doubleJumpValue = 1;
    private bool _doubleJumpIsReady;
    // end of jump section

    private Rigidbody2D rb;
    private Animator _anim;

    private void Start()
    {
        _anim = GetComponent<Animator>();
        _doubleJump = _doubleJumpValue;
        rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        numberOfHeats.text = hearts.ToString() + "x";

        JumpMovement();

        //death condition
        if(hearts <= 0)
        {
            SceneManager.LoadScene("Level2"); //level 2 is a final screen for no particular reason
        }
    }

    private void FixedUpdate()
    {
        _isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, thisIsGround);
        LeftMovement();
        RightMovement();

        if(_isGrounded == true)
        {
            _anim.SetBool("isRunning", true);
        }
    }

    private void LeftMovement()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Translate(Vector2.left * _backSpeed * Time.deltaTime);
        }
    }

    private void RightMovement()
    {
        if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.Translate(Vector2.right * _frontSpeed * Time.deltaTime);
        }
    }

    private void JumpMovement()
    {
        //double jump prep
        if(_isGrounded == true)
        {
            //_doubleJumpIsReady = true;
            _doubleJump = _doubleJumpValue;
            _anim.SetBool("isJumping", false);
        } else
        {
            _anim.SetBool("isJumping", true);
        }

        if (Input.GetKeyDown(KeyCode.UpArrow) && _doubleJump > 0 && _doubleJumpIsReady == true)
        {
            _anim.SetTrigger("takeOff");
            rb.velocity = Vector2.up * _jumpSpeed;
            _doubleJump--;
            _doubleJumpIsReady = false;
        }
       else if(Input.GetKeyDown(KeyCode.UpArrow) && _isGrounded == true)
        {
            _anim.SetTrigger("takeOff");
            rb.velocity = Vector2.up * _jumpSpeed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Obstackle01")
        {
            hearts -=1;
        }

        if (collision.gameObject.tag == "Bird")
        {
            hearts -= 1;
        }

        if (collision.gameObject.tag == "DeadlySpike")
        {
            hearts -= 1;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Heart")
        {
            hearts += 1;
        }
    }
}
