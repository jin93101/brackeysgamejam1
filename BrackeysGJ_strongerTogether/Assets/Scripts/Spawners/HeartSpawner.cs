﻿using UnityEngine;

public class HeartSpawner : MonoBehaviour
{
    private float _timeBtwSpawns;
    public float startTimeBtwSpawns;
    public GameObject[] heartCollection;

    private void Update()
    {
        if (_timeBtwSpawns <= 0)
        {
            int rand = Random.Range(0, heartCollection.Length);
            Instantiate(heartCollection[rand], transform.position, Quaternion.identity);
            _timeBtwSpawns = startTimeBtwSpawns;
        }
        else
        {
            _timeBtwSpawns -= Time.deltaTime;
        }
    }
}
