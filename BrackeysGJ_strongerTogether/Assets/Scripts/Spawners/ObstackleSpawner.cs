﻿using UnityEngine;

public class ObstackleSpawner : MonoBehaviour
{
    public GameObject[] spawnCollection;
    public float timer;
    public float startTimer = 15f;


    private void Update()
    {
         int numb = Random.Range(0, 3);
        switch (numb)
        {
            case 0:
                startTimer = 1;
                break;
            case 1:
                startTimer = 2;
                break;
            case 2:
                startTimer = 3;
                break;
            case 3:
                startTimer = 4;
                break;
        }

        if (timer > 0)
        { timer -= Time.deltaTime; }
        else
        {
            int rand = Random.Range(0, spawnCollection.Length);
            Instantiate(spawnCollection[rand], transform.position, Quaternion.identity);
            timer = startTimer;
        }
    }
}
