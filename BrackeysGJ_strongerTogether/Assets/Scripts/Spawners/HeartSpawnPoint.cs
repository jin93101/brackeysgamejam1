﻿using UnityEngine;

public class HeartSpawnPoint : MonoBehaviour
{
    public GameObject newHeart;
    private float _killMe = 2f;

    void Start()
    {
        Instantiate(newHeart, transform.position, Quaternion.identity);
    }
    private void Update()
    {
        _killMe -= Time.deltaTime;
        if (_killMe <= 0)
        {
            Destroy(gameObject);
        }
    }
}
