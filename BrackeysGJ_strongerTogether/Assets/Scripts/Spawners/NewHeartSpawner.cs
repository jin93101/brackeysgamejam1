﻿using UnityEngine;

public class NewHeartSpawner : MonoBehaviour
{
    private float _timeBtwSpawns;
    public float startTimeBtwSpawns;
    public GameObject[] spawnPoints;
    public int rand;

    private void Update()
    {
        if (_timeBtwSpawns <= 0)
        {
            rand = Random.Range(0, spawnPoints.Length);
            Instantiate(spawnPoints[rand], transform.position, Quaternion.identity);
            _timeBtwSpawns = startTimeBtwSpawns;
        }
        else
        {
            _timeBtwSpawns -= Time.deltaTime;
        }
    }
}
