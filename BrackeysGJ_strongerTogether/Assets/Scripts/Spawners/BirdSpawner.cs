﻿using UnityEngine;

public class BirdSpawner : MonoBehaviour
{
    public GameObject[] birdCollection;
    public float timer;
    public float startTimer = 15f;

    private void Update()
    {
        if(PuzzlePiece.score > 100)
        {

            int numb = Random.Range(0, 2);
            switch (numb)
            {
                case 0:
                    startTimer = 4;
                    break;
                case 1:
                    startTimer = 5;
                    break;
                case 2:
                    startTimer = 6;
                    break;
            }

            if (timer > 0)
            { timer -= Time.deltaTime; }
            else
            {
                int rand = Random.Range(0, birdCollection.Length);
                Instantiate(birdCollection[rand], transform.position, Quaternion.identity);
                timer = startTimer;
            } 
        }
    }
}
