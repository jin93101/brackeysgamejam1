﻿using UnityEngine;

public class DeadlySpikesSpawner : MonoBehaviour
{
    public GameObject[] spawnCollection;
    private float _timeBtwSpawns;
    public float startTimeBtwSpawns;
    public int rand;


    private void Update()
    {
        if (PuzzlePiece.score >= 125)
        {
            if (_timeBtwSpawns <= 0)
            {
                rand = Random.Range(0, spawnCollection.Length);
                Instantiate(spawnCollection[rand], transform.position, Quaternion.identity);
                _timeBtwSpawns = startTimeBtwSpawns;
            }
            else
            {
                _timeBtwSpawns -= Time.deltaTime;
            }
        }
    }
}
